import Vue from 'vue'
import Router from 'vue-router'
import LoginMain from '@/components/login/LoginMain'
import Home from '@/components/Home'

Vue.use(Router)
Vue.component('LoginMain', LoginMain)
Vue.component('Home', Home)


export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'LoginMain',
      component: LoginMain,
      meta: {
        permissions: []
      }
    },
    {
      path: '/login/:expired',
      component: LoginMain,
      name: 'LoginExpired',
      meta: {
        permissions: []
      }
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: LoginMain,
      meta: {
        permissions: []
      }
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        permissions: []
      }
    }
  ]
})

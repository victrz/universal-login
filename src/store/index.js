import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
// import Env from '../environment.js'
// import axios from 'axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    login: {
      token: '',
      accountToken: ''
      // user_id: null
      // app_id: '',
      // app_secret: ''
    },
    accounts: [],
    activeAccount: {},
    permissions: []
  },
  mutations: {
    SET_VUEX_STORAGE_TOKEN (state, data) {
      state.login.token = data.token
    },
    SET_VUEX_STORAGE_ACCOUNT_TOKEN (state, data) {
      console.log('account token is set')
      state.login.accountToken = data.account_token
    },
    CLEAR_VUEX_STORAGE (state) {
      state.login.token = ''
      state.login.accountToken = ''
      state.accounts = []
      state.activeAccount = {}
      state.permissions = []
    },
    ACCOUNTS (state, data) {
      state.accounts = data
    },
    ACTIVE_ACCOUNT (state, data) {
      state.activeAccount = data
    },
    ACCOUNT_PERMISSIONS (state, data) {
      state.permissions = data
    }
  },
  actions: {
    SET_STORAGE_TOKEN ({commit}, data) {
      commit('SET_VUEX_STORAGE_TOKEN', data)
    },
    SET_STORAGE_ACCOUNT_TOKEN ({commit}, data) {
      commit('SET_VUEX_STORAGE_ACCOUNT_TOKEN', data)
    },
    CLEAR_STORAGE ({commit}) {
      commit('CLEAR_VUEX_STORAGE')
    },
    SET_ACCOUNTS ({commit}, data) {
      commit('ACCOUNTS', data)
    },
    SET_ACTIVE_ACCOUNT ({commit}, data) {
      commit('ACTIVE_ACCOUNT', data)
    },
    SET_ACCOUNT_PERMISSIONS ({commit}, data) {
      commit('ACCOUNT_PERMISSIONS', data)
    }
  },
  getters: {
    getToken (state) {
      return state.login.token
    },
    getAccountToken (state) {
      return state.login.accountToken
    },
    getAccounts (state) {
      return state.accounts
    },
    getActiveAccount (state) {
      return state.activeAccount
    },
    getAccountPermissions (state) {
      return state.permissions
    }
  }
})

export default store
